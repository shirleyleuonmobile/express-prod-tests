package com.shirley.expressProdTests;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.view.KeyEvent;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.uiautomator.Until.findObject;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;


/**
 * Created by Shirley Leu on 23/05/16.
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)

public class InitiationTest {
    private static UiDevice mDevice;
    private static final String EXPRESS_PACKAGE = "com.onmobile.express";
    private static final int TIMEOUT = 10000;
    private static final int ANIMATION_SLEEP = 500;

    private static final String CORRECT_NUMBER = "06 08 51 01 90";

    private static final String FIRST_SONG = "We are the  champions";
    private static final String REPLACEMENT_SONG = "Glory Glory Man United";

    private static final String FIRST_CONTACT = "Adam Adrian";
    private static final String REPLACEMENT_CONTACT = "Ben Boris";
    private static final String MULTINUMBER_CONTACT = "Alex Alexandra";
    private static final String TOP_CONTACT = "Shirley";

    @Before
    public void startMainActivityFromHomeScreen() {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Start from the home screen
        mDevice.pressHome();

        // Wait for launcher
        final String launcherPackage = mDevice.getLauncherPackageName();
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), TIMEOUT);

        // Launch the blueprint app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(EXPRESS_PACKAGE);

        // Clear out any previous instances
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        // Wait for the app to appear
        mDevice.wait(Until.hasObject(By.pkg(EXPRESS_PACKAGE).depth(0)), TIMEOUT);
    }

    @After
    public void logout() throws UiObjectNotFoundException {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Start from the home screen
        mDevice.pressHome();

        // Wait for launcher
        final String launcherPackage = mDevice.getLauncherPackageName();
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), TIMEOUT);

        // Launch the blueprint app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(EXPRESS_PACKAGE);

        // Clear out any previous instances
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        // Wait for the app to appear
        mDevice.wait(Until.hasObject(By.pkg(EXPRESS_PACKAGE).depth(0)), TIMEOUT);

        UiObject2 profileTab = waitAndFind("icon_right");
        if(profileTab != null) {
            pressBackUntilCanViewAndClick("icon_right");

            // Click Account
            UiObject2 account2 = waitAndFind("account_picture");
            account2.click();

            // Scroll down
            UiScrollable settings = new UiScrollable(new UiSelector()
                    .className("android.widget.ScrollView"));
            UiObject logout = settings.getChildByText(
                    new UiSelector().className(android.widget.TextView.class.getName()), "Log out");

            // Click logout
            logout.click();
        }
    }
    @Test
    public void loginProcedure() throws UiObjectNotFoundException {
        loginUntilGuidedTour();

        // Assert that Guided Tour page opens
        UiObject2 ephemButton = waitAndFind("ephemeral_btn");
        UiObject2 defButton = waitAndFind("yes_btn");
        assertThat(ephemButton, is(notNullValue()));
        assertThat(defButton, is(notNullValue()));

        // Bypass Guided Tour
        UiObject2 goodButton = mDevice.wait(findObject(By.res(EXPRESS_PACKAGE, "good_btn")), TIMEOUT);
        goodButton.click();

        pressBackUntilCanViewAndClick("icon_right");
    }

    @Test // Bitbucket issue 38 - resolved
    public void pressBackEphemeralCategories() {
        loginUntilGuidedTour();

        // Click Ephemeral tone
        UiObject2 ephTone = waitAndFind("ephemeral_btn");
        ephTone.click();

        // Click next
        UiObject2 nextButton2 = waitAndFind("next_btn");
        nextButton2.click();

        // Click next again
        UiObject2 nextButton3 = waitAndFind("next_btn2");
        nextButton3.click();

        // Click Tap to pick a sound
        UiObject2 tapSound = waitAndFind("green_circle");
        tapSound.click();
        SystemClock.sleep(ANIMATION_SLEEP);

        // Press back
        mDevice.pressBack();

        // Assert Tap to pick a sound is there
        UiObject2 tapSound2 = waitAndFind("green_circle");
        assertThat(tapSound2, is(notNullValue()));
    }

    @Ignore // Bitbucket issue 114 - unresolved - feature removed
    public void verificationCodeSleepTest() throws RemoteException {
        UiObject2 startButton = waitAndFind("start_btn");
        // Click Start button
        startButton.click();

        // Next activity: Enter SIM number
        UiObject2 phoneNumber = waitAndFind("phone_number");
        phoneNumber.setText(CORRECT_NUMBER);

        // Click next
        UiObject2 nextButton = waitAndFind("next_btn");
        nextButton.click();

        // Click power button
        mDevice.sleep();

        // Click power button again
        mDevice.wakeUp();

        // Enter 1
        mDevice.pressKeyCode(KeyEvent.KEYCODE_1);

        // Assert that 1 was entered
        UiObject2 code1 = waitAndFind("code1");
        //String code1Text = code1.getText(); verify the code text when this issue is resolved
        assertThat(code1, is(notNullValue()));
    }

    @Test // Bitbucket issue 134 and 136 - resolved and unresolved, respectively
    public void  contentLanguage() throws UiObjectNotFoundException {
        SystemClock.sleep(ANIMATION_SLEEP);  // still having issues even with description contains

        loginUntilGuidedTour();

        // Bypass Guided Tour
        UiObject2 goodButton = mDevice.wait(findObject(By.res(EXPRESS_PACKAGE, "good_btn")), TIMEOUT);
        goodButton.click();

        // Press device back until can see the Profile tab and click it
        pressBackUntilCanViewAndClick("icon_right");

        // Click Account
        UiObject2 account = waitAndFind("account_picture");
        account.click();

        // Scroll down
        UiScrollable accountSettings = new UiScrollable(new UiSelector()
                .className("android.widget.ScrollView"));
        UiObject contentLanguage = accountSettings.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), "Content language");
        UiObject2 languageText = waitAndFind("language_text");

        // issue 134
        assertThat(languageText.getText(), is("Please select your content language"));

        contentLanguage.click();

        UiObject2 english = mDevice.wait(findObject(By.text("English")), TIMEOUT);
        english.click();

        // Go to Music track
        pressBackUntilCanViewAndClick("icon_middle");

        // issue 136 - this feature is no longer available 0.1.32
        UiObject2 contentLanguageWindowTitle = waitAndFind("alertTitle");
        assertThat(contentLanguageWindowTitle, is(nullValue()));
    }

    public static UiObject2 waitAndFind(String res) {
        return mDevice.wait(findObject(By.res(EXPRESS_PACKAGE, res)), TIMEOUT);
    }

    public void enterVerificationCode() {
        mDevice.pressKeyCode(KeyEvent.KEYCODE_1);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_2);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_3);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_4);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_4);
    }

    public void loginUntilGuidedTour() {
        UiObject2 startButton = waitAndFind("agree_continue");
        // Click Start button
        startButton.click();

        // Next activity: Enter SIM number
        UiObject2 phoneNumber = waitAndFind("phone_number");
        phoneNumber.setText(CORRECT_NUMBER);

        // Click next
        UiObject2 nextButton = waitAndFind("next_btn");
        nextButton.click();

        // Enter Verification code
        enterVerificationCode();

        // Click next
        mDevice.findObject(By.res(EXPRESS_PACKAGE, "next_btn2")).click();
    }

    public void sendMessageFromHistory() throws UiObjectNotFoundException {
        // Press device back until can see the History tab and click it
        pressBackUntilCanViewAndClick("icon_left");

        // Click +
        UiObject2 plusImage = waitAndFind("plus_image");
        plusImage.click();

        // Click Pick A Sound
        UiObject2 pickSound = waitAndFind("pick_sound_circle");
        pickSound.click();

        // Click a Song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject firstSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), FIRST_SONG);
        firstSong.click();

        // Click a Set for XXXX
        UiObject2 setSong = waitAndFind("use_it_now");
        setSong.click();

        // Click Pick A Contact
        UiObject2 pickContact = waitAndFind("pick_text");
        pickContact.click();

        // Click First Contact
        UiObject2 firstContact = mDevice.wait(findObject(By.text(FIRST_CONTACT)), TIMEOUT);
        firstContact.click();

        // Click Next
        UiObject2 nextBar = waitAndFind("selected_layout");
        nextBar.click();

        // Click Confirm
        UiObject2 confirmCheck = waitAndFind("confirm");
        confirmCheck.click();
    }

    public void sendMessageFromStore() throws UiObjectNotFoundException {
        // Press device back until can see the Store tab and click it
        pressBackUntilCanViewAndClick("icon_middle");

        pressBackUntilCanViewAndClick("icon_middle");

        // Click the First Song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject firstSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), FIRST_SONG);
        firstSong.click();

        // Click Next
        UiObject2 setForContact = waitAndFind("use_it_now");
        setForContact.click();

        // Click a Contact from Top Frequent view
        // Precondition: have contacts in phone
        UiScrollable contactList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject topContact = contactList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), TOP_CONTACT);
        topContact.click();

        // Click Next
        UiObject2 nextBar = waitAndFind("selected_layout");
        nextBar.click();

        // Click Confirm
        UiObject2 confirmCheck = waitAndFind("confirm");
        confirmCheck.click();
    }

    public void sendFromConversation(
            String song
    ) throws UiObjectNotFoundException {
        pressBackUntilCanViewAndClick("setRBTFab");

        // Click song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject replacementSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), song);
        replacementSong.click();

        // Click Set for XXXX
        UiObject2 setForContact = waitAndFind("use_it_now");
        setForContact.click();
    }

    public void verifyStatusSongContact(
            String status,
            String song,
            String contact) {
        // Find song title
        UiObject2 trackName = waitAndFind("track_name");
        String trackNameText = trackName.getText();

        // Find message status
        UiObject2 dateStatus = waitAndFind("date");
        String dateStatusText = dateStatus.getText();

        // Find contact name
        UiObject2 infoButton = waitAndFind("action_info");
        infoButton.click();
        UiObject2 contactButton = waitAndFind("action_contact");
        contactButton.click();
        UiObject2 contactName = mDevice.wait(findObject(By.res("com.android.contacts", "name")), TIMEOUT);
        String contactNameText = contactName.getText();

        // Assert pending status of track
        assertThat(dateStatusText, is(status));
        // Assert song title matches what was chosen
        assertThat(trackNameText, is(song));
        // Assert contact name
        assertThat(contactNameText, is(contact));
    }

    public static void pressBackUntilCanViewAndClick(
            String res){
        int tries = 0;
        while (tries < 5) {
            UiObject2 tabButton = waitAndFind(res);
            if (tabButton != null) {
                tabButton.click();
                break;
            }
            mDevice.pressBack();
            tries += 1;
        }
        if (tries  >= 5) {
            throw new IllegalStateException("Cannot find and click "+res);
        }
    }
}

