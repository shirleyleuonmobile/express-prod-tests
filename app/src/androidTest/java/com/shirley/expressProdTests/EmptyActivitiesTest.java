package com.shirley.expressProdTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;

import android.os.RemoteException;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.view.KeyEvent;

import static android.support.test.uiautomator.Until.findObject;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by Shirley Leu on 19/04/2016.
 * Working version on 11/May/2016
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)

public class EmptyActivitiesTest {
    private static final String EXPRESS_PACKAGE = "com.onmobile.express";
    private static final int TIMEOUT = 10000;
    private static final int ANIMATION_SLEEP = 500;

    private static final String CORRECT_NUMBER = "06 08 51 01 90";
    private static final String VERIFICATION_CODE = "12345";

    private static final String PENDING_STATUS = "pending";

    private static final String FIRST_SONG = "We are the  champions";
    private static final String REPLACEMENT_SONG = "Glory Glory Man United";

    private static final String FIRST_CONTACT = "Adam Adrian";
    private static final String REPLACEMENT_CONTACT = "Ben Boris";
    private static final String MULTINUMBER_CONTACT = "Alex Alexandra";
    private static final String TOP_CONTACT = "Shirley";


    private static UiDevice mDevice;

    @Before
    public void startMainActivityFromHomeScreen() {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Start from the home screen
        mDevice.pressHome();

        // Wait for launcher
        final String launcherPackage = mDevice.getLauncherPackageName();
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), TIMEOUT);

        // Launch the blueprint app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(EXPRESS_PACKAGE);

        // Clear out any previous instances
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        // Wait for the app to appear
        mDevice.wait(Until.hasObject(By.pkg(EXPRESS_PACKAGE).depth(0)), TIMEOUT);

        // Bypass login if already logged in
        UiObject2 startButton = waitAndFind("agree_continue");
        if (startButton != null) {
            // Click Start button
            startButton.click();

            // Next activity: Enter SIM number
            UiObject2 phoneNumber = waitAndFind("phone_number");
            phoneNumber.setText(CORRECT_NUMBER);

            // Click next
            UiObject2 nextButton = waitAndFind("next_btn");
            nextButton.click();

            // Enter Verification code
            enterVerificationCode();

            // Click next
            mDevice.findObject(By.res(EXPRESS_PACKAGE, "next_btn2")).click();

            // Bypass Guided Tour if it appears
            UiObject2 goodButton = mDevice.wait(findObject(By.res(EXPRESS_PACKAGE, "good_btn")), TIMEOUT);
            if (goodButton != null && goodButton.isEnabled()) {
                goodButton.click();
            }

            // Bypass popups
            pressBackUntilCanViewAndClick("icon_middle");
        }
        else {
            // Bypass Guided Tour if it appears
            UiObject2 goodButton = mDevice.wait(findObject(By.res(EXPRESS_PACKAGE, "good_btn")), TIMEOUT);
            if (goodButton != null && goodButton.isEnabled()) {
                goodButton.click();
            }

            // Bypass Guided Tour and trophy popups
            pressBackUntilCanViewAndClick("icon_middle");
        }
        // Overrides language popup
        pressBackUntilCanViewAndClick("icon_middle");
    }

    @After
    public void logout() {
        // Press device  back until can see the Profile tab and click it
        // pressBackUntilCanViewAndClick("icon_right");
        // SystemClock.sleep(ANIMATION_SLEEP);

        /*
        // Click Account
        UiObject2 account = waitAndFind("account");
        account.click();

        // Scroll down
        UiScrollable settings = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject logout = settings.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), "Logout");

        // Click logout
        logout.click();*/
    }

    @Test // Bitbucket issue 118 - resolved
    public void defaultHeaderEmptyHistoryCheck() {
        // Go to History tab
        pressBackUntilCanViewAndClick("icon_left");

        // Check if Default tone header is present
        UiObject2 editButton = waitAndFind("edit_button");
        assertThat(editButton, is(notNullValue()));
    }

    @Test // Bitbucket issue 186 - resolved
    public void clearTrophyNotifAfterClick() {
        // Press device back until can see the Profile tab and click it
        pressBackUntilCanViewAndClick("icon_right");

        // Click "Newbie" trophy
        UiObject2 newbieTrophy = mDevice.findObject(By.text("Newbie"));
        newbieTrophy.click();

        SystemClock.sleep(ANIMATION_SLEEP);

        // Close trophy window
        mDevice.pressBack();

        // Confirm that notification badge is gone
        UiObject2 trophyNotif = waitAndFind("trophy_notif");
        assertThat(trophyNotif,is(nullValue()));
    }

    @Ignore // view no longer available -Bitbucket issue 131 - resolved
    public void headerTextBlankFirstMsg() throws UiObjectNotFoundException {
        // Go to History tab
        pressBackUntilCanViewAndClick("icon_left");

        // Click "Pick A Contact"
        UiObject2 pickContact = waitAndFind("pick_contact");
        SystemClock.sleep(ANIMATION_SLEEP);
        pickContact.click();

        // Click Contact
        UiObject2 firstContact = mDevice.wait(findObject(By.text(FIRST_CONTACT)), TIMEOUT);
        firstContact.click();

        // Click "Pick A Sound"
        UiObject2 pickSound = waitAndFind("pick_sound");
        pickSound.click();

        // Click the First Song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject firstSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), FIRST_SONG);
        firstSong.click();

        // Click Set for XXXX
        UiObject2 setForContact = waitAndFind("use_it_now");
        setForContact.click();

        // Verify if there is text
        UiObject2 toolbarTitle = waitAndFind("toolbar_title");
        assertThat(toolbarTitle, is(notNullValue())); // check text of this once typos are fixed
    }

    @Ignore // view no longer available - Bitbucket issue 49 - resolved
    public void multipleNumbersFirstMessageNext() throws UiObjectNotFoundException {
        // Go to Activity tab
        pressBackUntilCanViewAndClick("icon_left");

        // Click "Pick A Contact"
        SystemClock.sleep(ANIMATION_SLEEP);  //not solved even with content description
        UiObject2 pickContact = waitAndFind("pick_contact");
        pickContact.click();

        // Click multi-number contact
        UiObject2 multiContact = mDevice.wait(findObject(By.text(MULTINUMBER_CONTACT)), TIMEOUT);
        multiContact.click();

        // Click a number checkbox
        UiObject2 numberCheckbox = waitAndFind("number_checkbox");
        numberCheckbox.click();

        // Find Pick A Sound button
        UiObject2 pickSound = waitAndFind("pick_sound");

        // Assert window changes to Pick A Sound
        assertThat(pickSound, is(notNullValue()));
    }


    public static UiObject2 waitAndFind(String res) {
        return mDevice.wait(findObject(By.res(EXPRESS_PACKAGE, res)), TIMEOUT);
    }

    public static void enterVerificationCode() {
        mDevice.pressKeyCode(KeyEvent.KEYCODE_1);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_2);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_3);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_4);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_4);
    }

    public static void pressBackUntilCanViewAndClick(
            String res){
        int tries = 0;
        while (tries < 3) {
            UiObject2 tabButton = waitAndFind(res);
            if (tabButton != null) {
                tabButton.click();
                break;
            }
            mDevice.pressBack();
            tries += 1;
        }
        if (tries  >= 5) {
            throw new IllegalStateException("Cannot find and click "+res);
        }
    }
}