package com.shirley.expressProdTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;

import android.os.RemoteException;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.view.KeyEvent;

import static android.support.test.uiautomator.Until.findObject;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by Shirley Leu on 19/04/2016.
 * Working version on 11/May/2016
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)

public class LoggedInTest {
    private static final String EXPRESS_PACKAGE = "com.onmobile.express";
    private static final int TIMEOUT = 5000;
    private static final int RECORD_TIME = 400;
    private static final int ANIMATION_SLEEP = 500;
    private static final int SONG_DURATION = 35000;


    private static final String CORRECT_NUMBER = "06 08 51 01 90";
    private static final String VERIFICATION_CODE = "1234";

    private static final String PENDING_STATUS = "pending";

    private static final String FIRST_SONG = "We are the  champions";
    private static final String REPLACEMENT_SONG = "Glory Glory Man United";

    private static final String FIRST_CONTACT = "Adam Adrian";
    private static final String REPLACEMENT_CONTACT = "ALLO";
    private static final String MULTINUMBER_CONTACT = "Alex Alexandra";
    private static final String TOP_CONTACT = "Office";

    private static UiDevice mDevice;

    @BeforeClass
    public static void login() {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Start from the home screen
        mDevice.pressHome();

        // Wait for launcher
        final String launcherPackage = mDevice.getLauncherPackageName();
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), TIMEOUT);

        // Launch the blueprint app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(EXPRESS_PACKAGE);

        // Clear out any previous instances
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        // Wait for the app to appear
        mDevice.wait(Until.hasObject(By.pkg(EXPRESS_PACKAGE).depth(0)), TIMEOUT);

        // Bypass login if already logged in
        UiObject2 startButton = waitAndFind("agree_continue");
        if (startButton != null) {
            // Click Start button
            startButton.click();

            // Next activity: Enter SIM number
            UiObject2 phoneNumber = waitAndFind("phone_number");
            phoneNumber.setText(CORRECT_NUMBER);

            // Click next
            UiObject2 nextButton = waitAndFind("next_btn");
            nextButton.click();

            // Enter Verification code
            enterVerificationCode();
            // InstrumentationRegistry.getInstrumentation().sendStringSync(VERIFICATION_CODE);

            // Click next
            UiObject2 nextButton2 = waitAndFind("next_btn2");
            nextButton2.click();

            // Bypass Guided Tour if it appears
            UiObject2 goodButton = mDevice.wait(findObject(By.res(EXPRESS_PACKAGE, "good_btn")), TIMEOUT);
            if (goodButton != null && goodButton.isEnabled()) {
                goodButton.click();
            }

            // Bypass popups
            pressBackUntilCanViewAndClick("icon_middle");
        }
        else {
            // Bypass Guided Tour if it appears
            UiObject2 goodButton = waitAndFind("good_btn");
            if (goodButton != null && goodButton.isEnabled()) {
                goodButton.click();
            }

            // Bypass Guided Tour and trophy popups
            pressBackUntilCanViewAndClick("icon_middle");
        }
        // Overrides language popup
        pressBackUntilCanViewAndClick("icon_middle");
    }

    @Before
    public void startMainActivityFromHomeScreen() {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Wait for launcher
        final String launcherPackage = mDevice.getLauncherPackageName();
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), TIMEOUT);

        // Launch the blueprint app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(EXPRESS_PACKAGE);

        // Clear out any previous instances
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        // Wait for the app to appear
        mDevice.wait(Until.hasObject(By.pkg(EXPRESS_PACKAGE).depth(0)), TIMEOUT);
    }

    @After
    public void profileClick() throws UiObjectNotFoundException {
        // Press device  back until can see the Profile tab and click it
        pressBackUntilCanViewAndClick("icon_right");
        // SystemClock.sleep(ANIMATION_SLEEP);


        // Click Account
        // UiObject2 account = waitAndFind("account"); // Name of this resource has changed
        //account.click();

        // Scroll down
        //UiScrollable settings = new UiScrollable(new UiSelector()
        //        .className("android.support.v7.widget.RecyclerView"));
        //UiObject logout = settings.getChildByText(
        //        new UiSelector().className(android.widget.TextView.class.getName()), "Logout");

        // Click logout
        //logout.click();
    }

    @AfterClass
    public static void logout() throws UiObjectNotFoundException {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Start from the home screen
        mDevice.pressHome();

        // Wait for launcher
        final String launcherPackage = mDevice.getLauncherPackageName();
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), TIMEOUT);

        // Launch the blueprint app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(EXPRESS_PACKAGE);

        // Clear out any previous instances
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        // Wait for the app to appear
        mDevice.wait(Until.hasObject(By.pkg(EXPRESS_PACKAGE).depth(0)), TIMEOUT);

        UiObject2 profileTab = waitAndFind("icon_right");
        if(profileTab != null) {
            pressBackUntilCanViewAndClick("icon_right");

            // Click Account
            UiObject2 account2 = waitAndFind("account_picture");
            account2.click();

            // Scroll down
            UiScrollable settings = new UiScrollable(new UiSelector()
                    .className("android.widget.ScrollView"));
            UiObject logout = settings.getChildByText(
                    new UiSelector().className(android.widget.TextView.class.getName()), "Log out");

            // Click logout
            logout.click();
        }
    }

    @Test
    public void checkPreconditions() {
        assertThat(mDevice, notNullValue());
    }

    @Test
    public void sendMessageFromHistoryTestAndReplaceFromConversation() throws UiObjectNotFoundException {
        sendMessageFromHistory();
        verifyStatusSongContact(PENDING_STATUS, FIRST_SONG, FIRST_CONTACT);

        // Replace message from inside conversation
        sendFromConversation(REPLACEMENT_SONG);
        // Assert the resulting message conversation
        verifyStatusSongContact(PENDING_STATUS, REPLACEMENT_SONG, FIRST_CONTACT);
    }

    @Test
    public void sendMessageFromStoreTest() throws UiObjectNotFoundException {
        SystemClock.sleep(ANIMATION_SLEEP);

        // Press device back until can see the Store tab and click it
        pressBackUntilCanViewAndClick("icon_middle");

        // Click the First Song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject firstSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), FIRST_SONG);
        firstSong.click();

        // Click Set for contact
        UiObject2 setForContact = waitAndFind("use_it_now");
        setForContact.click();

        // Click a Contact from Top Frequent view
        UiScrollable contactList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject topContact = contactList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), TOP_CONTACT);
        topContact.click();

        // Click Next
        UiObject2 nextBar = waitAndFind("selected_layout");
        nextBar.click();

        // Click Confirm
        UiObject2 confirmCheck = waitAndFind("confirm");
        confirmCheck.click();

        // Subscription pop-up close
        acceptSubscriptionPopUp();

        SystemClock.sleep(ANIMATION_SLEEP);

        verifyStatusSongContact(PENDING_STATUS, FIRST_SONG, TOP_CONTACT);
    }

    @Ignore // - Record button removed
    public void sendRecordedMessageTest() throws UiObjectNotFoundException {
        // Go to Music tab
        pressBackUntilCanViewAndClick("icon_middle");

        // Click and hold Record button
        UiObject recordButton = mDevice.findObject(new UiSelector().resourceId("com.onmobile.express:id/white_circle"));
        Rect rectButton = recordButton.getBounds();
        mDevice.swipe(rectButton.centerX(), rectButton.centerY(), rectButton.centerX(), rectButton.centerY(), RECORD_TIME);

        // Click Set for contact
        UiObject2 setButton = waitAndFind("use_it_now");
        setButton.click();

        // Click contact
        UiObject2 firstContact = mDevice.wait(findObject(By.text(FIRST_CONTACT)), TIMEOUT);
        firstContact.click();

        // Click Next
        UiObject2 nextBar = waitAndFind("selected_layout");
        nextBar.click();

        // Click Confirm
        UiObject2 confirmCheck = waitAndFind("confirm");
        confirmCheck.click();

        // Assert the resulting message conversation
        verifyStatusSongContact(PENDING_STATUS, "Your record", FIRST_CONTACT);
    }

    @Test // Bitbucket issue 33 - resolved
    public void checkOneSearchBarDefaultTone() throws UiObjectNotFoundException {
        // Click History
        pressBackUntilCanViewAndClick("icon_left");

        // Click Edit under Default Tone
        UiObject2 editButton = waitAndFind("edit_button_default");
        if (editButton != null) {
            editButton.click();
        }
        else {
            UiObject2 editButton2 = waitAndFind("edit_button");
            editButton2.click();
        }

        // Click Music Tone
        UiScrollable toneList = new UiScrollable(new UiSelector()
                .className("android.widget.LinearLayout"));
        toneList.flingForward();
        UiObject musicTone = toneList.getChildByText(new UiSelector().resourceId("com.onmobile.express:id/category_name"), "Music");
        //UiObject2 musicTone = waitAndFind("music_tone");
        musicTone.click();

        // Assert that former second search bar is not present
        UiObject2 searchSoundNameCat = waitAndFind("search_edit_text");
        assertThat(searchSoundNameCat, is(nullValue()));
    }

    @Test // Bitbucket issue 69 - resolved
    public void pickContactTextDisappearsAfterCancelMultipleContactMessage() throws UiObjectNotFoundException {
        // SystemClock.sleep(ANIMATION_SLEEP);

        // Click History
        pressBackUntilCanViewAndClick("icon_left");

        // Click +
        UiObject2 plusImage = waitAndFind("plus_image");
        plusImage.click();

        // Click Pick A Sound
        UiObject2 pickSound = waitAndFind("pick_sound_circle");
        pickSound.click();

        // Click First Song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject firstSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), FIRST_SONG);
        firstSong.click();

        // Click Set for Contact
        UiObject2 setForContact = waitAndFind("use_it_now");
        setForContact.click();

        // Click Pick a Contact
        UiObject2 pickContact = mDevice.wait(findObject(By.descContains("pick contact")), TIMEOUT);
        pickContact.click();

        // Click two contacts
        UiObject2 firstContact = mDevice.wait(findObject(By.text(FIRST_CONTACT)), TIMEOUT);
        firstContact.longClick();

        UiObject2 replacementContact = mDevice.wait(findObject(By.text(REPLACEMENT_CONTACT)), TIMEOUT);
        replacementContact.longClick();

        // Click Next
        UiObject2 nextBar = waitAndFind("selected_layout");
        nextBar.click();

        // Click Cancel
        UiObject2 cancel = waitAndFind("cancel");
        cancel.click();

        // Verify if Pick A Contact is displayed
        UiObject2 pickText = waitAndFind("pick_text");
        assertThat(pickText, is(notNullValue()));
        assertThat(pickText.getText(), is("Pick a"));
    }

    @Test // Bitbucket issue 133 - resolved
    public void accountGearIconInactive() throws UiObjectNotFoundException {
        SystemClock.sleep(ANIMATION_SLEEP);

        // Go to Profile tab
        pressBackUntilCanViewAndClick("icon_right");

        // Click gear icon
        UiObject2 gearIcon = waitAndFind("settings_icon");
        gearIcon.click();

        // Assert that account is open
        UiObject2 accountHeader = mDevice.wait(findObject(By.text("Account")), TIMEOUT);
        assertThat(accountHeader, is(notNullValue()));
    }


    @Test // Bitbucket issue 122 - depends on issue 118, but is resolved
    public void defaultToneAfterUndo() throws UiObjectNotFoundException {
        // Go to History tab
        pressBackUntilCanViewAndClick("icon_left");

        // Obtain default tone name
        UiObject2 currentTone = waitAndFind("default_title");
        String currentToneName = currentTone.getText();

        // Go to Music tab
        pressBackUntilCanViewAndClick("icon_middle");

        SystemClock.sleep(ANIMATION_SLEEP); // need or else cannot find song later

        // Click First Song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        //UiScrollable trackList = new UiScrollable(new UiSelector()
        //        .resourceId("com.onmobile.express:id/recycler_view"));
        UiObject firstSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), FIRST_SONG);
        firstSong.click();

        // Click Set for everyone
        UiObject2 setEveryone = waitAndFind("set_for_everyone");
        setEveryone.click();

        UiObject2 undoButton = waitAndFind("snackbar_action");
        if (undoButton != null) {
            undoButton.click();
        } else {
            UiObject2 startTrialButton = waitAndFind("button_start2");
            if (startTrialButton != null) {
                startTrialButton.click();

                UiObject2 subscribeButton = waitAndFind("button_subscribe");
                subscribeButton.click();
            }
        }

        // Go to History tab
        pressBackUntilCanViewAndClick("icon_left");

        // Obtain default tone name
        UiObject2 defaultTone = waitAndFind("default_title");
        String defaultToneName = defaultTone.getText();

        // Assert the default tone has not been changed
        assertThat(defaultToneName, equalTo(currentToneName));
    }

    @Test // Bitbucket issue 68 - resolved but test is flaky
    public void clickNextContactList() throws UiObjectNotFoundException {
        // Press device back until can see the History tab and click it
        pressBackUntilCanViewAndClick("icon_left");

        // Click +
        UiObject2 plusImage = waitAndFind("plus_image");
        plusImage.click();

        //Click Pick A Contact
        SystemClock.sleep(ANIMATION_SLEEP); // need or else get stale object exception
        UiObject2 pickContact = waitAndFind("pick_contact");
        //UiObject2 pickContact = mDevice.wait(findObject(By.descContains("pick contact")), TIMEOUT);
        pickContact.click();

        // Click First Contact
        UiObject2 firstContact = mDevice.wait(findObject(By.text(FIRST_CONTACT)), TIMEOUT);
        firstContact.click();

        // Click Next
        UiObject2 nextBarText = waitAndFind("selected_contact_number");
        nextBarText.click();

        // Assert only "First Contact" was chosen
        SystemClock.sleep(ANIMATION_SLEEP); // need or else get stale object exception
        UiObject2 contactName = waitAndFind("contact_name");
        assertThat(contactName.getText(), is(equalTo(FIRST_CONTACT)));
    }


    @Test // Bitbucket issue 30 - unresolved
    public void previewPlayPausePowerButton() throws UiObjectNotFoundException, RemoteException {
        // Go to Music tab
        pressBackUntilCanViewAndClick("icon_middle");

        // Click song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject firstSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), FIRST_SONG);
        firstSong.click();

        // Device sleep
        mDevice.sleep();

        // Device wake
        mDevice.wakeUp();

        // Assert that play button is set to play
        UiObject2 playButton = waitAndFind("preview");
        assertThat(playButton.isSelected(), is(false));
    }

    @Test // Bitbucket issue 173 - resolved
    public void guidedTourTonePlayPausePowerButton() throws UiObjectNotFoundException, RemoteException {
        SystemClock.sleep(ANIMATION_SLEEP);

        // Go to Profile tab
        pressBackUntilCanViewAndClick("icon_right");

        // Click Account
        UiObject2 account = waitAndFind("account_picture");
        account.click();

        // Scroll down
        UiScrollable settings = new UiScrollable(new UiSelector()
                .className("android.widget.ScrollView"));
        UiObject guidedTour = settings.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), "Guided tour");

        // Click Guided tour
        guidedTour.click();

        // Click Default tone
        UiObject2 defTone = waitAndFind("yes_btn");
        defTone.click();

        // Click next
        UiObject2 nextButton = waitAndFind("next_btn");
        nextButton.click();

        // Device sleep
        mDevice.sleep();

        // Device wake
        mDevice.wakeUp();

        // Assert that play button is set to play
        UiObject2 playButton = waitAndFind("play_pause_button");
        assertThat(playButton.isSelected(), is(false));
    }

    @Test // Bitbucket issue 137 - resolved
    public void sendWithSearch() {
        pressBackUntilCanViewAndClick("icon_left");

        // Click +
        UiObject2 plusImage = waitAndFind("plus_image");
        plusImage.click();

        //Click Pick A Contact
        UiObject2 pickContact = waitAndFind("pick_text");
        //UiObject2 pickContact = mDevice.wait(findObject(By.descContains("pick contact")), TIMEOUT); strange that it can no longer find it this way
        pickContact.click();

        // Click First Contact
        UiObject2 firstContact = mDevice.wait(findObject(By.text(FIRST_CONTACT)), TIMEOUT);
        firstContact.click();

        // Click Next
        UiObject2 nextBarText = waitAndFind("selected_contact_number");
        nextBarText.click();

        // Click Pick A Sound
        UiObject2 pickSound = waitAndFind("pick_sound_circle");
        pickSound.click();

        // Click Search sound
        UiObject2 search = waitAndFind("search_contact_layout");
        search.click();

        // Enter text
        UiObject2 searchText = waitAndFind("search_src_text");
        searchText.setText("work");

        // Click Search
        mDevice.pressEnter(); // do not use Google Keyboard. Use Huawei Swype

        // Click any track
        UiObject2 anySong = waitAndFind("imageView1");
        anySong.click();

        // Check Set for text
        //SystemClock.sleep(ANIMATION_SLEEP);
        UiObject2 setText = waitAndFind("use_it_now_text");
        assertThat(setText.getText(), is(equalTo("Set for " + FIRST_CONTACT)));
    }

    @Test // Bitbucket issue 177 - resolved
    public void searchApostrophe() {
        SystemClock.sleep(ANIMATION_SLEEP);
        pressBackUntilCanViewAndClick("icon_middle");

        // Click Search sound
        UiObject2 search = waitAndFind("search_ll");
        search.click();

        // Enter apostrophe
        UiObject2 searchText = waitAndFind("search_src_text");
        searchText.setText("'");
    }

    @Test // Bitbucket issue 154 - unresolved
    public void searchContactTwoCloseX() throws RemoteException {
        pressBackUntilCanViewAndClick("icon_left");

        // Click +
        UiObject2 plusImage = waitAndFind("plus_image");
        plusImage.click();

        //Click Pick A Contact
        UiObject2 pickContact = waitAndFind("pick_text");
        pickContact.click();

        // Click Search
        UiObject2 searchBar = waitAndFind("toolbar");
        searchBar.click();

        // Enter text
        UiObject2 searchBarEditText = waitAndFind("search_src_text");
        searchBarEditText.setText("test");

        // Press recent key twice
        mDevice.pressRecentApps();
        SystemClock.sleep(ANIMATION_SLEEP);
        mDevice.pressRecentApps();

        // Assert that second "x" did not appear
        UiObject2 innerX = waitAndFind("search_close_btn");
        assertThat(innerX, is(nullValue()));
    }

    @Test // Bitbucket issue 49 - resolved
    public void multipleNumbersCheckboxMessageNext() throws UiObjectNotFoundException {
        // Go to Activity tab
        pressBackUntilCanViewAndClick("icon_left");

        // Click +
        UiObject2 plusImage = waitAndFind("plus_image");
        plusImage.click();

        // Click "Pick A Contact"
        SystemClock.sleep(ANIMATION_SLEEP);  //not solved even with content description
        UiObject2 pickContact = waitAndFind("pick_contact");
        pickContact.click();

        // Click multi-number contact
        UiObject2 multiContact = mDevice.wait(findObject(By.text(MULTINUMBER_CONTACT)), TIMEOUT);
        multiContact.click();

        // Click a number checkbox
        UiObject2 numberCheckbox = waitAndFind("number_checkbox");
        numberCheckbox.click();

        // Find Next bar
        UiObject2 nextBar = waitAndFind("selected_layout");

        // Assert that Next is available
        assertThat(nextBar, is(notNullValue()));
    }

    @Test // Bitbucket issue 219 - resolved
    public void previewEndTrackPlaying() throws UiObjectNotFoundException {
        SystemClock.sleep(ANIMATION_SLEEP);
        // Go to Music tab
        pressBackUntilCanViewAndClick("icon_middle");

        //SystemClock.sleep(ANIMATION_SLEEP); // need or else cannot find song later

        // Click song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject firstSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), FIRST_SONG);
        firstSong.click();

        // Wait until song finishes playing
        SystemClock.sleep(SONG_DURATION);

        // Check if the preview button is back to "play"
        UiObject2 previewButton = waitAndFind("preview");
        assertThat(previewButton.isSelected(), is(false));
    }

    @Test // Bitbucket issue 236 - unresolved
    public void guidedTourONMOJinglePausePlay() throws UiObjectNotFoundException, RemoteException {
        // Press device back until can see the Profile tab and click it
        pressBackUntilCanViewAndClick("icon_right");

        // Click Account
        UiObject2 account = waitAndFind("account_picture");
        account.click();

        // Scroll down
        UiScrollable settings = new UiScrollable(new UiSelector()
                .className("android.widget.ScrollView"));
        UiObject guidedTour = settings.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), "Guided tour");

        // Click Guided tour
        guidedTour.click();

        // Click Default ONMO
        UiObject2 defTone = waitAndFind("yes_btn");
        defTone.click();

        SystemClock.sleep(ANIMATION_SLEEP);  // need this pause here

        // Device sleep
        mDevice.sleep();

        SystemClock.sleep(ANIMATION_SLEEP);  // need this pause here

        // Device wake
        mDevice.wakeUp();

        // Click next
        UiObject2 nextButton = waitAndFind("next_btn");
        nextButton.click();

        // Assert that play/pause button is playing
        UiObject2 playButton = waitAndFind("play_pause_button");
        assertThat(playButton.isSelected(), is(true));
    }

    @Test // Bitbucket issue 238 - unresolved
    public void defaultHeaderONMOJinglePlayPause() throws RemoteException {
        // Go to History tab
        pressBackUntilCanViewAndClick("icon_left");

        // Click Default ONMO header
        UiObject2 defaultHeader = waitAndFind("normal_layout");
        defaultHeader.click();

        SystemClock.sleep(ANIMATION_SLEEP);  // need this pause here to find ONMO Jingles

        // Click ONMO Jingles
        UiObject2 ONMOJingles = mDevice.findObject(By.res("com.onmobile.express:id/category_name").text("Jingles"));
        ONMOJingles.click();

        // Device sleep
        mDevice.sleep();

        // Device wake
        mDevice.wakeUp();

        // Assert that play/pause button is not playing
        UiObject2 playButton = waitAndFind("play_pause_button");
        assertThat(playButton.isSelected(), is(false));
    }

    @Test // Bitbucket issue 255 - unresolved
    public void clickChangePlan() {
        SystemClock.sleep(ANIMATION_SLEEP);

        // Go to Profile tab
        pressBackUntilCanViewAndClick("icon_right");

        // Click Account
        UiObject2 account = waitAndFind("account_picture");
        account.click();

        // Click Change Plan
        UiObject2 changePlan = mDevice.wait(findObject(By.clazz("android.widget.TextView").text("Change plan")), TIMEOUT);
        changePlan.click();

        // Click Current Plan
        UiObject2 currentPlan = mDevice.wait(findObject(By.clazz("android.widget.TextView").text("Current plan")), TIMEOUT);
        currentPlan.click();

        // Assert that view has not changed
        UiObject2 missYou = mDevice.wait(findObject(By.res("text1").text("We'll miss you")), TIMEOUT);
        UiObject2 startTrial = waitAndFind("button_start2");
        if (missYou != null) {
            assertThat(missYou, is(nullValue()));
        } else if (startTrial != null) {
            assertThat(startTrial, is(nullValue()));
        } else {
            UiObject2 currentPlan2 = mDevice.wait(findObject(By.clazz("android.widget.TextView").text("Current plan")), TIMEOUT);
            assertThat(currentPlan2, is(notNullValue()));
        }
    }

    @Ignore // Bitbucket issue 63 - resolved -- need help to find the individual checkboxes
    public void multinumberCheckbox() {
        // Go to Activity tab
        pressBackUntilCanViewAndClick("icon_left");

        // Click +
        UiObject2 plusImage = waitAndFind("plus_image");
        plusImage.click();

        // Click "Pick A Contact"
        SystemClock.sleep(ANIMATION_SLEEP);  //not solved even with content description
        UiObject2 pickContact = waitAndFind("pick_contact");
        pickContact.click();

        // Click multi-number contact
        UiObject2 multiContact = mDevice.wait(findObject(By.text(MULTINUMBER_CONTACT)), TIMEOUT);
        multiContact.click();

        // Click bottom number checkbox
        UiObject2 secondNumber = mDevice.wait(findObject(By.text("+33645131684")), TIMEOUT);
        secondNumber.click();

        // Click top number checkbox
        UiObject2 topNumber = mDevice.wait(findObject(By.text("+33645131684")), TIMEOUT);
        topNumber.click();

        // Click name to collapse view
        UiObject2 multinumberContact = mDevice.wait(findObject(By.res("contact_name").text(MULTINUMBER_CONTACT)),TIMEOUT);
        multinumberContact.click();

        // Check if parent checkbox is checked

    }

    @Ignore // Bitbucket issue 31 - resolved - need help finding View All button
    public void trophyList() throws UiObjectNotFoundException {
        // Press device back until can see the Profile tab and click it
        pressBackUntilCanViewAndClick("icon_right");

        // Scroll down
        UiScrollable profileList = new UiScrollable(new UiSelector()
                .className("android.widget.ScrollView"));
        // profileList.flingForward();
        //profileList.scrollTextIntoView("View all");
        UiObject viewAll = profileList.getChildByText(
                new UiSelector().resourceId("com.onmobile.express:id/view_all"), "View all");
        //UiObject2 viewAll = waitAndFind("view_all");
        viewAll.click();

        //UiScrollable profileList = new UiScrollable(new UiSelector()
        //        .className("android.widget.ScrollView"));
        //int cnt = profileList.getChildCount();
        //for(int i = 0; i < cnt; i++) {
        //    UiObject viewAll = profileList.getChild(new UiSelector().index(i));
        //}

        assertThat(viewAll, is(notNullValue()));
    }

    @Ignore // Bitbucket issue 22 - need finding correct play-pause button
    public void guidedTourDevotionTest() throws UiObjectNotFoundException {
        // Press device back until can see the Profile tab and click it
        pressBackUntilCanViewAndClick("icon_right");

        // Click Account
        UiObject2 account = waitAndFind("account_picture");
        account.click();

        // Scroll down
        UiScrollable settings = new UiScrollable(new UiSelector()
                .className("android.widget.ScrollView"));
        UiObject guidedTour = settings.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), "Guided tour");

        // Click Guided tour
        guidedTour.click();

        // Click Default tone
        UiObject2 defTone = waitAndFind("yes_btn");
        defTone.click();

        // Click next
        UiObject2 nextButton = waitAndFind("next_btn");
        nextButton.click();

        // Swipe right to the end
        //UiScrollable listTones = new UiScrollable(new UiSelector()
        //        .resourceId("com.onmobile.express:id/root"));
        UiScrollable listTones = new UiScrollable(new UiSelector().className("android.support.v4.view.ViewPager"));
        listTones.setAsHorizontalList();
        listTones.scrollToEnd(10);

        // Try to identify Devotion status
        //SystemClock.sleep(ANIMATION_SLEEP);

        // UiObject2 devotionPlay = waitAndFind("play_pause_button");
        UiObject devotionPlay = mDevice.findObject(new UiSelector().resourceId("com.onmobile.express:id/play_pause_button").index(9));

        assertThat(devotionPlay, is(notNullValue()));
        devotionPlay.click();

        SystemClock.sleep(2000);

        assertThat(devotionPlay.isSelected(), is(false));
    }

    @Ignore // Critical issue - resolved - need help finding toggle button
    public void drDreCutCrash() throws UiObjectNotFoundException {
        // Press device back until can see the Store tab and click it
        pressBackUntilCanViewAndClick("icon_middle");

        pressBackUntilCanViewAndClick("icon_middle");

        // Click the Dr Dre song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject dreSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), "What's the difference");
        dreSong.click();

        UiObject2 cutIcon = waitAndFind("preview_close");
        cutIcon.click();

        // Click and drag Cut toggle
        UiObject cutToggle = mDevice.findObject(new UiSelector().resourceId("com.onmobile.express:id/white_circle"));
        Rect rectButton = cutToggle.getBounds();
        mDevice.swipe(rectButton.centerX(), rectButton.centerY(), rectButton.centerX(), rectButton.centerY(), RECORD_TIME);
    }

    public static UiObject2 waitAndFind(String res) {
        return mDevice.wait(findObject(By.res(EXPRESS_PACKAGE, res)), TIMEOUT);
    }

    public static void enterVerificationCode() {
        mDevice.pressKeyCode(KeyEvent.KEYCODE_1);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_2);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_3);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_4);
        mDevice.pressKeyCode(KeyEvent.KEYCODE_4);
    }

    public void sendMessageFromHistory() throws UiObjectNotFoundException {
        // Press device back until can see the History tab and click it
        pressBackUntilCanViewAndClick("icon_left");

        // Click +
        UiObject2 plusImage = waitAndFind("plus_image");
        plusImage.click();

        // Click Pick A Sound
        UiObject2 pickSound = waitAndFind("pick_sound_circle");
        pickSound.click();

        // Click a Song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject firstSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), FIRST_SONG);
        firstSong.click();

        // Click a Set for XXXX
        UiObject2 setSong = waitAndFind("use_it_now");
        setSong.click();

        //Click Pick A Contact
        UiObject2 pickContact = waitAndFind("pick_text");
        pickContact.click();

        // Click First Contact
        UiObject2 firstContact = mDevice.wait(findObject(By.text(FIRST_CONTACT)), TIMEOUT);
        firstContact.click();

        // Click Next
        UiObject2 nextBar = waitAndFind("selected_layout");
        nextBar.click();

        // Click Confirm
        UiObject2 confirmCheck = waitAndFind("confirm");
        confirmCheck.click();

        // Subscription pop-up close
        acceptSubscriptionPopUp();
    }

    public void sendMessageFromStore() throws UiObjectNotFoundException {
        // Press device back until can see the Store tab and click it
        pressBackUntilCanViewAndClick("icon_middle");

        // Click the First Song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject firstSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), FIRST_SONG);
        firstSong.click();

        // Click Set for contact
        UiObject2 setForContact = waitAndFind("use_it_now");
        setForContact.click();

        // Click a Contact from Top Frequent view
        UiScrollable contactList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject topContact = contactList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), TOP_CONTACT);
        topContact.click();

        // Click Next
        UiObject2 nextBar = waitAndFind("selected_layout");
        nextBar.click();

        // Click Confirm
        UiObject2 confirmCheck = waitAndFind("confirm");
        confirmCheck.click();

        // Subscription pop-up close
        acceptSubscriptionPopUp();
    }

    public void sendFromConversation(
            String song
    ) throws UiObjectNotFoundException {
        pressBackUntilCanViewAndClick("change_onmo");

        // Click song
        UiScrollable trackList = new UiScrollable(new UiSelector()
                .className("android.support.v7.widget.RecyclerView"));
        UiObject replacementSong = trackList.getChildByText(
                new UiSelector().className(android.widget.TextView.class.getName()), song);
        replacementSong.click();

        // Click Set for XXXX
        UiObject2 setForContact = waitAndFind("use_it_now");
        setForContact.click();
    }

    public void verifyStatusSongContact(
            String status,
            String song,
            String contact) throws UiObjectNotFoundException {

        SystemClock.sleep(ANIMATION_SLEEP);

        // Find song title
        UiObject2 trackName = waitAndFind("track_name");
        String trackNameText = trackName.getText();

        // Find message status
        UiObject2 dateStatus = waitAndFind("date");
        String dateStatusText = dateStatus.getText();

        // Find contact name
        UiObject2 infoButton = waitAndFind("action_info");
        if (infoButton != null) {
            infoButton.click();
        }
        UiObject2 contactButton = waitAndFind("action_contact");
        contactButton.click();
        UiObject2 contactName = mDevice.wait(findObject(By.res("com.android.contacts", "name")), TIMEOUT); // on LG G4, the resource ID is photoName
        String contactNameText = contactName.getText();

        // Assert pending status of track
        // assertThat(dateStatusText, is(status)); // This will fail if contact has called before - remove temporarily 6/9/16
        // Assert song title matches what was chosen
        assertThat(trackNameText, is(song));
        // Assert contact name
        assertThat(contactNameText, is(contact));
    }

    public static void pressBackUntilCanViewAndClick(
            String res){
        int tries = 0;
        while (tries < 5) {
            UiObject2 tabButton = waitAndFind(res);
            if (tabButton != null) {
                tabButton.click();
                break;
            }
            mDevice.pressBack();
            tries += 1;
        }
        if (tries  >= 5) {
            throw new IllegalStateException("Cannot find and click "+res);
        }
    }
    public void acceptSubscriptionPopUp() {
        UiObject2 startTrialButton = waitAndFind("button_start2");
        if (startTrialButton != null) {
            startTrialButton.click();

            UiObject2 subscribeButton = waitAndFind("button_subscribe");
            subscribeButton.click();
        }
    }
}